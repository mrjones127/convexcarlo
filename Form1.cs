﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using MIConvexHull;
using ConvexCarlo.PRNG;
/*
 * Isaac Jones
 * A test of monte-carlo point distribution in a polygon
 *
 */
namespace ConvexCarlo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }
        int seedGlobal;
        private void pictureBox_output_Click(object sender, EventArgs e)
        {
            //lots of hardcoded values - change later?
            int mapwidth = pictureBox_output.Width;
            int mapheight = pictureBox_output.Height;
            Vertex[] simplexCircleOut = simplexCircle(200, mapwidth, mapheight, 100, 0.5f, seedGlobal);
            DefaultVertex[] hullPoints = new DefaultVertex[simplexCircleOut.Length];
            for (int i = 0; i < simplexCircleOut.Length; i++)
                hullPoints[i] = new DefaultVertex { Position = new [] {(double)simplexCircleOut[i].X, simplexCircleOut[i].Y } };
            var hull = ConvexHull.Create(hullPoints);
            hullPoints = hull.Points.ToArray();
            Vertex[] hullVertices = new Vertex[hull.Points.Count()];
            for (int i = 0; i < hullVertices.Length; i++)
                hullVertices[i] = new Vertex((float)hullPoints[i].Position[0], (float)hullPoints[i].Position[1]);
            Vertex[] biomeSeeds = monteCarlo(hullVertices, 10, mapwidth, mapheight, seedGlobal);
            displayPoints(simplexCircleOut, biomeSeeds, mapwidth, mapheight);
            seedGlobal++;
        }
        public bool isPointInPolygon(Vertex testPoint, Vertex[] polygon)
        {
            bool result = false;
            int j = polygon.Count() - 1;
            for (int i = 0; i < polygon.Count(); i++)
            {
                if (polygon[i].Y < testPoint.Y && polygon[j].Y >= testPoint.Y || polygon[j].Y < testPoint.Y && polygon[i].Y >= testPoint.Y)
                    if (polygon[i].X + (testPoint.Y - polygon[i].Y) / (polygon[j].Y - polygon[i].Y) * (polygon[j].X - polygon[i].X) < testPoint.X)
                        result = !result;
                j = i;
            }
            return result;
        }

        public float sampleCircleX (float t, float radius, float startX)
        {
            //have t be from 0 to 2pi
            return startX + radius * (float)Math.Pow(Math.Cos(t), .8);
        }
        public float sampleCircleY(float t, float radius, float startY)
        {
            //have t be from 0 to 2pi
            return startY + radius * (float)Math.Pow(Math.Sin(t), 3);
        }
        //simplex multiplier goes from 0 to 1 times the radius of the circle
        public Vertex[] simplexCircle(float scale, int mapwidth, int mapheight, int sampleCount, float simplexMultiplier, int seed)
        {
            //ALWAYS 1
            float radius = 1;
            float ellipseX = 2;
            float ellipseY = 1;
//            OpenSimplexNoise openSimplex = new OpenSimplexNoise(seed);
//            float circumference = radius * 2 * (float)Math.PI;
            float startX = (mapwidth * 0.5f) - scale;
            float startY = (mapheight * 0.5f) - scale;
            List<Vertex> circlePoints = new List<Vertex>();
            for (int i = 0; i < sampleCount; i++)
            {
                float x = ((sampleCircleX((2 * (float)Math.PI * (i * 1.00000f / sampleCount)), radius * ellipseX, 0)));
                float y = ((sampleCircleY((2 * (float)Math.PI * (i * 1.00000f / sampleCount)), radius * ellipseY, 0)));
                circlePoints.Add(new Vertex(startX + (x + radius) * scale, startY + (y + radius) * scale));
                circlePoints.Add(new Vertex(startX + (-x + radius) * scale, startY + (y + radius) * scale));
                circlePoints.Add(new Vertex(startX + (x + radius) * scale, startY + (-y + radius) * scale));
                circlePoints.Add(new Vertex(startX + (-x + radius) * scale, startY + (-y + radius) * scale));
                //should never happen
//                if (circlePoints.Any(p => p.X < 0 || p.Y < 0))
//                    Text = "WHY";
            }
            //using opensimplex to change point distribution pseudorandomly
            //for (int i = 0; i < circlePoints.Count; i++)
            //{
            //    float multiplier = 1 + (float)(openSimplex.eval(circlePoints[i].x, circlePoints[i].y) * simplexMultiplier);
            //    circlePoints[i].x *= multiplier;
            //    circlePoints[i].y *= multiplier;
            //}
            return circlePoints.ToArray();
        }
        public Vertex[] monteCarlo(Vertex[] polygonVertices, int pointCount, int mapwidth, int mapheight, int seed)
        {
            Well19937c Well = new Well19937c(seed);
            Vertex[] points = new Vertex[pointCount * 5000];
            for (int i = 0; i < points.Length; i++)
                points[i] = new Vertex(Well.nextFloat() * mapwidth, Well.nextFloat() * mapheight);
            List<Vertex> pointsInPolygon = new List<Vertex>();
            for (int i = 0; i < points.Length; i++)
                if (isPointInPolygon(points[i], polygonVertices))
                    pointsInPolygon.Add(points[i]);
            Vertex[] finalPoints = new Vertex[pointCount];
            //pseudoranom point limiter
//            int www = (int)Math.Floor(Well.nextFloat() * pointsInPolygon.Count);
            pointsInPolygon = pointsInPolygon.OrderBy(p => p.X * p.X + p.Y * p.Y).ToList();
            for (int i = 0; i < finalPoints.Length; i++)
                finalPoints[i] = pointsInPolygon[(int)Math.Floor(Well.nextFloat() * pointsInPolygon.Count)];
            return finalPoints;
            //return points;
        }
        public void displayPoints(Vertex[] points, Vertex[] biomeSeeds, int mapwidth, int mapheight)
        {
            PointF[] dPoints = new PointF[points.Length];
            for (int i = 0; i < points.Length; i++)
                dPoints[i] = new PointF(points[i].X, points[i].Y);
            PointF[] dSeeds = new PointF[biomeSeeds.Length];
            for (int i = 0; i < biomeSeeds.Length; i++)
                dSeeds[i] = new PointF(biomeSeeds[i].X, biomeSeeds[i].Y);
            Bitmap outmap = new Bitmap(mapwidth, mapheight);
            Graphics g = Graphics.FromImage(outmap);
            g.FillRectangle(new SolidBrush(Color.Beige), new RectangleF(0, 0, mapwidth, mapheight));
            for (int i = 0; i < points.Length; i++)
                g.FillRectangle(new SolidBrush(Color.Red), new RectangleF(dPoints[i], new SizeF(3, 3)));
            for (int i = 0; i < biomeSeeds.Length; i++)
                g.FillRectangle(new SolidBrush(Color.Green), new RectangleF(dSeeds[i], new SizeF(3, 3)));
            pictureBox_output.Image = outmap;
        }
    }
}