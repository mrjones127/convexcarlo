﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvexCarlo
{
    public class Vertex
    {
        public float X;
        public float Y;
        public Vertex(float X, float Y)
        {
            this.X = X;
            this.Y = Y;
        }
    }
}
